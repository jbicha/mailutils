This is mailutils.info, produced by makeinfo version 5.9.93 from
mailutils.texi.

Published by the Free Software Foundation, 51 Franklin Street, Fifth
Floor Boston, MA 02110-1301, USA

   Copyright (C) 1999-2004, 2008-2012, 2014-2018 Free Software
Foundation, Inc.

   Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover, and no Back-Cover texts.  A copy of
the license is included in the section entitled "GNU Free Documentation
License".
INFO-DIR-SECTION Email
START-INFO-DIR-ENTRY
* Mailutils: (mailutils).       GNU Mail Utilities.
END-INFO-DIR-ENTRY

INFO-DIR-SECTION Individual utilities
START-INFO-DIR-ENTRY
* comsatd: (mailutils) comsatd.          Comsat Daemon.
* frm: (mailutils) frm.                  List Headers from a Mailbox.
* guimb: (mailutils) guimb.              Mailbox Processing Language.
* imap4d: (mailutils) imap4d.            IMAP4 Daemon.
* mail: (mailutils) mail.                Send and Receive Mail.
* maidag: (mailutils) maidag.            A General-Purpose Mail Delivery Agent.
* messages: (mailutils) messages.        Count Messages in a Mailbox.
* movemail: (mailutils) movemail.        Move Mail between Mailboxes.
* pop3d: (mailutils) pop3d.              POP3 Daemon.
* readmsg: (mailutils) readmsg.          Extract Messages from a Folder.
* sieve: (mailutils) sieve.              Mail Filtering Utility.
* mimeview: (mailutils) mimeview.        View MIME Messages.
* mailutils: (mailutils) mailutils.      Mailutils Multi-Purpose Tool
END-INFO-DIR-ENTRY


Indirect:
mailutils.info-1: 1685
mailutils.info-2: 302833

Tag Table:
(Indirect)
Node: Top1685
Node: Introduction10454
Node: Book Contents14072
Node: History15553
Node: Mailbox16469
Node: Local Mailboxes17489
Ref: mbox18045
Node: Remote Mailboxes19475
Node: SMTP Mailboxes22599
Node: Program Mailboxes25000
Node: Programs26170
Node: command line27776
Node: Option Basics28036
Node: Common Options31169
Node: configuration34463
Node: conf-syntax40318
Node: Comments40862
Node: Statements41538
Ref: boolean value42182
Ref: backslash-interpretation42748
Ref: here-document44111
Ref: list values45682
Node: Paths46524
Ref: the --set option48354
Node: Variables50109
Node: include52898
Ref: Include53063
Node: program statement53807
Node: logging statement54924
Ref: Logging Statement55105
Ref: syslog facility56004
Node: debug statement56878
Ref: Debug Statement57055
Node: mailbox statement57738
Ref: Mailbox Statement57919
Ref: local URL parameters59220
Ref: mailbox-type63540
Node: locking statement64176
Ref: Locking Statement64358
Node: mailer statement66637
Ref: Mailer Statement66813
Node: acl statement68126
Ref: ACL Statement68303
Node: tcp-wrappers statement73023
Ref: Tcp-wrappers Statement73217
Node: Server Settings75209
Node: General Server Configuration76186
Ref: server mode76925
Node: Server Statement79272
Node: auth statement84100
Ref: Auth Statement84270
Node: pam statement89411
Ref: PAM Statement89572
Node: virtdomain statement90041
Ref: Virtdomain Statement90230
Node: radius statement92286
Ref: Radius Statement92467
Node: sql statement97235
Ref: SQL Statement97404
Ref: getpw column names100325
Node: ldap statement102419
Ref: LDAP Statement102587
Ref: ldap statement-Footnote-1105222
Node: tls statement105360
Ref: TLS Statement105538
Node: tls-file-checks statement106551
Node: gsasl statement108201
Ref: GSASL Statement108362
Node: debugging108884
Node: Level Syntax110046
Node: Level BNF111392
Node: Debugging Categories112342
Node: frm and from116877
Node: mail120297
Node: Invoking Mail122176
Node: Specifying Messages126740
Node: Composing Mail129699
Node: Quitting Compose Mode130933
Node: Getting Help on Compose Escapes131826
Node: Editing the Message132241
Node: Modifying the Headers132934
Node: Enclosing Another Message133642
Node: Adding a File to the Message134364
Node: Attaching a File to the Message134778
Node: Printing And Saving the Message136189
Node: Signing the Message136633
Node: Printing Another Message137242
Node: Inserting Value of a Mail Variable137890
Node: Executing Other Mail Commands138252
Node: Executing Shell Commands139209
Node: MIME139857
Node: Reading Mail148853
Node: Quitting the Program151080
Node: Obtaining Online Help151864
Node: Moving Within a Mailbox152452
Node: Changing mailbox/directory152844
Node: Controlling Header Display153585
Node: Displaying Information154767
Node: Displaying Messages156072
Node: Marking Messages158208
Node: Disposing of Messages158860
Node: Saving Messages159630
Node: Editing Messages161881
Node: Aliasing162575
Node: Replying163652
Node: Controlling Sender Fields166297
Node: Incorporating New Mail167357
Node: Shell Escapes167762
Node: Scripting168262
Ref: Setting and Unsetting the Variables168809
Node: Mail Variables172408
Ref: byname174133
Ref: datefield174482
Ref: fromfield179217
Ref: headline179645
Ref: return-address192547
Node: Mail Configuration Files195494
Node: messages196390
Node: movemail197594
Node: Movemail Configuration198995
Ref: movemail-program-id199885
Ref: mailbox-ownership-methods201213
Ref: movemail-onerror201990
Node: Ownership203142
Node: Summary203652
Ref: Summary-Footnote-1206008
Node: readmsg206048
Node: Opt-readmsg207413
Ref: weedlist option208044
Node: Conf-readmsg208270
Node: sieve209520
Node: sieve interpreter210196
Node: Invoking Sieve210773
Node: Sieve Configuration214083
Node: Logging and Debugging216787
Node: Extending Sieve218777
Node: guimb221997
Node: Specifying Scheme Program to Execute224109
Node: Specifying Mailboxes to Operate Upon225142
Node: Passing Options to Scheme226307
Node: Command Line Option Summary227651
Node: maidag228759
Node: Sendmail-maidag230476
Node: Exim-maidag231821
Node: MeTA1-maidag232417
Node: Mailbox Quotas233412
Node: DBM Quotas234652
Node: SQL Quotas236045
Node: Maidag Scripting238126
Node: Sieve Maidag Filters239096
Node: Scheme Maidag Filters239938
Node: Python Maidag Filters240411
Node: Forwarding241822
Node: Url-mode243497
Node: Remote Mailbox Delivery243903
Node: Conf-maidag246664
Node: mimeview249604
Ref: mimeview-Footnote-1250841
Node: Mimeview Invocation251056
Node: Mimeview Config254704
Node: pop3d255230
Node: Login delay255946
Node: Auto-expire257990
Node: Bulletins259758
Node: Conf-pop3d261274
Node: Command line options263403
Node: imap4d264086
Node: Namespace264520
Node: Conf-imap4d270943
Node: Starting imap4d276276
Node: comsatd277344
Node: Starting comsatd277809
Node: Configuring comsatd278536
Node: General Settings279174
Node: Security Settings279623
Node: dot.biffrc280388
Node: mh283346
Node: Diffs284012
Node: Format String Diffs284884
Ref: decode function285084
Ref: reply_regex function286274
Ref: isreply MH function286967
Node: Profile Variable Diffs289049
Ref: Charset variable289235
Ref: Reply-Regex variable289429
Node: Program Diffs289715
Node: mailutils298640
Node: mailutils invocation syntax299973
Node: mailutils help302833
Node: mailutils info303507
Node: mailutils cflags305398
Node: mailutils ldflags305770
Node: mailutils stat307343
Node: mailutils query309709
Node: mailutils 2047311404
Node: mailutils filter313115
Node: mailutils acl315142
Node: mailutils wicket317979
Node: mailutils dbm319702
Node: Create a Database320471
Node: Add Records to a Database322670
Node: Delete Records323427
Node: List the Database325015
Node: Dump the Database325769
Node: Dump Formats326769
Ref: dump version 0.0327316
Ref: dump version 1.0328062
Node: Dbm Exit Codes329476
Node: mailutils logger330809
Node: mailutils pop332745
Node: mailutils imap338776
Ref: mailutils imap-Footnote-1343695
Ref: mailutils imap-Footnote-2343748
Ref: mailutils imap-Footnote-3343796
Ref: mailutils imap-Footnote-4343855
Ref: mailutils imap-Footnote-5343914
Ref: mailutils imap-Footnote-6343973
Node: mailutils send344032
Node: mailutils smtp345068
Node: Libraries348464
Node: Sieve Language348817
Node: Lexical Structure349222
Node: Syntax354359
Node: Commands354664
Node: Actions Described356202
Node: Control Flow356766
Node: Tests and Conditions358204
Node: Preprocessor359302
Node: #include359949
Node: #searchpath360621
Node: Require Statement360991
Node: Comparators364759
Node: Tests365385
Node: Built-in Tests369326
Node: External Tests374752
Node: Actions381064
Node: Built-in Actions381477
Ref: fileinto382532
Node: External Actions388038
Node: Extensions394598
Node: encoded-character394947
Node: relational397197
Node: variables398881
Node: environment402368
Node: numaddr404267
Node: editheader404492
Node: list405946
Node: moderator406117
Node: pipe406386
Node: spamd406680
Node: timestamp406942
Node: vacation407331
Node: GNU Extensions407645
Node: Reporting Bugs409820
Node: News410420
Node: Acknowledgement410987
Node: References411579
Node: Date Input Formats411944
Node: General date syntax414300
Node: Calendar date items417252
Node: Time of day items419255
Node: Time zone items421458
Node: Day of week items422697
Node: Relative items in date strings423691
Node: Pure numbers in date strings426499
Node: Seconds since the Epoch427486
Node: Specifying time zone rules429114
Node: Authors of get_date431485
Node: Date/time Format String432243
Ref: %c time format433456
Ref: %s time format436444
Ref: conversion specs438584
Node: Usage Vars439250
Node: GNU FDL444897
Node: Function Index467326
Node: Variable Index491190
Node: Keyword Index497311
Node: Program Index518365
Node: Concept Index519538

End Tag Table
